const express = require('express');
const axios = require('axios');

const app = express();
const PORT = 3001; // You can change the port number if needed

// Middleware to allow CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

// Endpoint for translating words
app.post('/translate', async (req, res) => {
  const { wordsArray, targetLanguage } = req.body;

  try {
    const response = await axios.post(
      'https://api.openai.com/v1/translate',
      {
        text: wordsArray,
        target_language: targetLanguage,
      },
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer YOUR_OPENAI_API_KEY',
        },
      }
    );

    const translations = response.data.translations;
    res.json({ translations });
  } catch (error) {
    console.error('Translation error:', error.message);
    res.status(500).json({ error: 'Translation failed' });
  }
});

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
