import AppColors from '../Colors';
import { getColor } from '../utils';
import stylesModal from '../ModalStyles';

const QuizStyles = (props) => ({
  container: {
    height: '100%',
  },
  modalView: {
    backgroundColor: getColor(props.mode, 'white'),
    padding: 20,
    paddingTop: 40,
    height: '100%',
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonSubmit: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 14,
    backgroundColor: getColor(props.mode, 'default'),
    borderRadius: 10,
  },
  buttonText: {
    fontSize: 24,
    textAlign: 'center',
    color: getColor(props.mode, 'white'),
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    marginLeft: 0,
    color: getColor(props.mode, 'black'),
  },
  question: {
    fontSize: 22,
    marginVertical: 10,
    color: getColor(props.mode, 'black'),
  },
  questionBold: {
    fontWeight: 'bold',
  },
  option: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    marginVertical: 7,
    borderWidth: 1,
    color: getColor(props.mode, 'black'),
    borderColor: getColor(props.mode, 'default'),
    borderRadius: 10,
  },
  optionText: {
    fontSize: 18,
    textAlign: 'center',
    color: getColor(props.mode, 'black'),
  },
  selectedOption: {
    backgroundColor: getColor(props.mode, 'default'),
  },
  selectedOptionText: {
    color: getColor(props.mode, 'white'),
  },
  feedback: {
    position: 'absolute',
    left: 20,
    top: -3,
    fontSize: 20,
    color: getColor(props.mode, 'black'),
  },
  score: {
    fontSize: 18,
    marginVertical: 10,
    color: getColor(props.mode, 'black'),
  },
  buttonClose: {
    backgroundColor: getColor(props.mode, 'default'),
    borderRadius: 50,
    position: 'absolute',
    right: 20,
    top: 20,
  },
  disabledButton: {
    backgroundColor: getColor(props.mode, 'gray'),
  },
  correctButton: {
    backgroundColor: AppColors.light.green,
    borderColor: AppColors.light.green,
  },
  incorrectButton: {
    backgroundColor: AppColors.light.red,
  },
  progressBarContainer: {
    width: '100%',
    backgroundColor: getColor(props.mode, 'gray'),
    borderRadius: 10,
    overflow: 'hidden',
  },
  progressBar: {
    height: '100%',
    height: 20,
    backgroundColor: AppColors.light.green,
    borderRadius: 10,
  },
  progressText: {
    marginTop: 5,
    fontSize: 16,
    color: getColor(props.mode, 'default'),
    marginBottom: 20,
  },  
  ...stylesModal,
});

export default QuizStyles;
