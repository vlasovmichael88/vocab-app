import React, { useState, useEffect } from 'react';
import { 
  View,
  Text,
  TouchableOpacity,
  Modal,
  Pressable
} from 'react-native';
import { getColor } from '../utils';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import QuizStyles from './styles';

const Quiz = ({ ...props }) => {
  const { words, mode, handleQuizCompletion } = props;
  const styles = QuizStyles(props);

  const [question, setQuestion] = useState({});
  const [options, setOptions] = useState([]);
  const [selectedOption, setSelectedOption] = useState(null);
  const [feedback, setFeedback] = useState('');
  const [score, setScore] = useState(0);
  const [isAnswered, setIsAnswered] = useState(false);
  const [isCorrect, setIsCorrect] = useState(false);
  const [usedQuestions, setUsedQuestions] = useState([]);
  const [progress, setProgress] = useState({ answered: 0, total: 0 });

  useEffect(() => {
    
      setScore(0);
      setProgress({ answered: 0, total: Object.entries(words).length }); // Initialize progress
      // Reset used questions to start fresh for a new session
      setUsedQuestions([]);
      generateQuiz();
    
  }, [words]);

  const generateQuiz = () => {
    const wordsArray = Object.entries(words);
  
    // Filter out used questions
    const availableWords = wordsArray.filter(([key]) => !usedQuestions.includes(key));
  
    // Check if there are enough questions
    if (availableWords.length < 4) {
      setFeedback('You have answered all the questions! Great job! 🎉');
      return;
    }
  
    // Select a random word from availableWords
    const randomIndex = Math.floor(Math.random() * availableWords.length);
    const [selectedKey, selectedWord] = availableWords[randomIndex];
  
    // Filter for other options
    const otherOptions = wordsArray
      .filter(([key]) => key !== selectedKey)
      .sort(() => 0.5 - Math.random())
      .slice(0, 3)
      .map(([_, word]) => word.translation);
  
    // Ensure sufficient options
    if (otherOptions.length < 3) {
      setFeedback('Not enough distinct options to generate a quiz. Add more words!');
      return;
    }
  
    // Shuffle all options
    const allOptions = [selectedWord.translation, ...otherOptions].sort(
      () => 0.5 - Math.random()
    );
  
    setQuestion(selectedWord);
    setOptions(allOptions);
    setFeedback('');
    setSelectedOption(null);
    setIsAnswered(false);
    setIsCorrect(false);
  
    // Mark question as used
    setUsedQuestions((prevUsedQuestions) => [...prevUsedQuestions, selectedKey]);
  };  

  const handleOptionSelect = (option) => {
    if (isAnswered) return;

    setSelectedOption(option);
    if (option === question.translation) {
      setFeedback('Correct! 🎉');
      setIsAnswered(true);
      setScore((prevScore) => prevScore + 1);
      setIsCorrect(true);
      handleQuizCompletion(true);
      // Update progress
      setProgress((prevProgress) => ({
        ...prevProgress,
        answered: prevProgress.answered + 1,
      }));
    } else {
      setFeedback('Oops! Try again. ❌');
      setIsCorrect(false);
      handleQuizCompletion(false);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.modalView}>
        <Text style={styles.feedback}>{feedback}</Text>
        <View style={styles.progressBarContainer}>
          <View
            style={[
              styles.progressBar,
              { width: `${(progress.answered / progress.total) * 100}%` },
            ]}
          />
        </View>
        <Text style={styles.progressText}>
          {progress.answered}{'/'}{progress.total} Questions Answered
        </Text>
        <Text style={styles.title}>Quiz</Text>
        <Text style={styles.score}>Score: {score}</Text>
        {options.length > 0 && (
          <>
            <Text style={styles.question}>
              What is the translation of{' '}
              <Text style={styles.questionBold}>{question.name}</Text>?
            </Text>
            {options.map((option, index) => (
              <TouchableOpacity
                key={index}
                style={[
                  styles.option,
                  selectedOption === option && styles.selectedOption,
                  isAnswered && option === question.translation && styles.correctButton,
                  isAnswered && option !== question.translation && { opacity: 0.5 },
                ]}
                disabled={isAnswered}
                onPress={() => handleOptionSelect(option)}
              >
                <Text style={[
                  styles.optionText,
                  selectedOption === option && styles.selectedOptionText,
                ]}>{option}</Text>
              </TouchableOpacity>
            ))}
            <Pressable 
              onPress={generateQuiz}
              disabled={selectedOption === null || !isCorrect} // Disable if no option selected or answer is incorrect
              style={[
                styles.button,
                styles.buttonSubmit,
                selectedOption === null
                  ? styles.disabledButton
                  : isCorrect
                  ? styles.correctButton
                  : styles.incorrectButton,
              ]}
            >
              <Text style={styles.buttonText}>
                Next Question
              </Text>
            </Pressable>
          </>
        )}
      </View>
    </View>
)};

export default Quiz;
