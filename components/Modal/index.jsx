import {
  StyleSheet,
  Pressable,
  Modal,
  View,
  Text,
  TextInput,
} from 'react-native';
import { getColor } from '../utils';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import VocabStyles from './styles';

const VocabModal = ({ ...props }) => {
  const styles = VocabStyles(props);

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={props.modalVisible}
      onRequestClose={props.openModal}
      style={styles.modalRoot}
    >
      <View style={styles.modalContainer}>
        <View style={styles.modalView}>
          <TextInput
            style={styles.input}
            placeholderTextColor={getColor(props.mode, 'default')}
            onChangeText={props.duplicatedWord}
            value={props.name}
            autoFocus={true}
            placeholder="Word"
          />
          <TextInput
            style={styles.input}
            placeholderTextColor={getColor(props.mode, 'default')}
            onChangeText={props.setTranslation}
            value={props.translation}
            placeholder="Main translation (optional)"
          />
          <TextInput
            style={styles.input}
            placeholderTextColor={getColor(props.mode, 'default')}
            onChangeText={(text) => props.setTag(text)}
            value={props.tag}
            placeholder="Tag (optional)"
          />
          <View style={styles.tagButtonContainer}>
            {props.tagArray.map(
              (item) =>
                item.length > 0 && (
                  <Pressable
                    key={item}
                    style={styles.tagButton}
                    onPress={() => props.setTag(item)}
                  >
                    <Text style={styles.tagButtonText}>{`#${item}`}</Text>
                  </Pressable>
                )
            )}
          </View>
          {props.isAddOrEdit ? (
            <Pressable
              style={[styles.button, styles.buttonSubmit]}
              onPress={props.addData}
            >
              <Text style={styles.buttonSubmitText}>{'Save'}</Text>
            </Pressable>
          ) : (
            <Pressable
              style={[styles.button, styles.buttonSubmit]}
              onPress={props.editData}
            >
              <Text style={styles.buttonSubmitText}>{'Edit'}</Text>
            </Pressable>
          )}
          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={props.openModal}
          >
            <MaterialCommunityIcons
              name="close"
              color={getColor(props.mode, 'white')}
              size={32}
            />
          </Pressable>
        </View>
      </View>
    </Modal>
  );
};

export default VocabModal;
