import stylesModal from '../ModalStyles';
import { getColor } from '../utils';

const VocabStyles = (props) => ({
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: getColor(props.mode, 'default'),
    position: 'absolute',
    bottom: 60,
    right: 30,
    borderRadius: 50,
  },
  input: {
    height: 60,
    fontSize: 18,
    marginBottom: 12,
    borderWidth: 1,
    borderColor: getColor(props.mode, 'default'),
    padding: 10,
    width: '100%',
    color: getColor(props.mode, 'black'),
    borderRadius: 10,
  },
  textError: {
    color: getColor(props.mode, 'red'),
    fontSize: 22,
    marginBottom: 15,
  },
  buttonSubmitText: {
    fontSize: 24,
    textAlign: 'center',
    color: getColor(props.mode, 'white'),
  },
  buttonSubmit: {
    marginTop: 20,
    width: '100%',
    padding: 10,
    backgroundColor: getColor(props.mode, 'default'),
    borderRadius: 10,
  },
  tagButtonContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  tagButton: {
    padding: 10,
    backgroundColor: getColor(props.mode, 'primary'),
    borderRadius: 10,
    marginRight: 15,
    marginBottom: 15,
    flexShrink: 1,
  },
  tagButtonText: {
    color: getColor(props.mode, 'white'),
    fontSize: 18,
  },
  // modal styles dublicted
  buttonClose: {
    backgroundColor: getColor(props.mode, 'default'),
    borderRadius: 50,
    position: 'absolute',
    right: 20,
    top: 20,
  },
  modalView: {
    width: '100%',
    height: '100%',
    backgroundColor: getColor(props.mode, 'white'),
    borderRadius: 20,
    padding: 35,
    paddingTop: 120,
    marginTop: 75 ,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  ...stylesModal,
});

export default VocabStyles;
