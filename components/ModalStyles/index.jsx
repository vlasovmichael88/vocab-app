import { StyleSheet } from 'react-native';

const stylesModal = StyleSheet.create({
  modalContainer: {
    height: '100%',
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
});

export default stylesModal;
