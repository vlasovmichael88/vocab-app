import { getColor } from '../utils';

const HomeStyles = (props) => ({
  container: {
    backgroundColor: getColor(props.mode, 'white'),
    height: '100%',
  },
});

export default HomeStyles;
