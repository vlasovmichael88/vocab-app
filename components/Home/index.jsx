import React from 'react';
import { View } from 'react-native';
import PlusIcon from '../PlusIcon';
import VocabModal from '../Modal';
import WordList from '../WordList';
import RemoveItemModal from '../RemoveItemModal';
import HomeStyles from './styles';

const Home = ({...props}) => {
  const styles = HomeStyles(props);

  const {
    word,
    wordsKeys,
    openEditModal,
    removeItem,
    removeItemModalFn,
    isAddOrEdit,
    modalVisible,
    openModal,
    addData,
    editData,
    setName,
    setTag,
    tagArray,
    setTranslation,
    tag,
    name,
    duplicatedWord,
    isRemoveItemModal,
    id,
    translation,
    filteredWord,
    setSearch,
    handleCloseIcon,
    handleSearchChange,
    searchInput,
    setSearchInput,
    isCloseIcon,
    loading,
  } = props;

  return (
    <View style={styles.container}>
      <WordList
        word={word}
        wordsKeys={wordsKeys}
        openEditModal={openEditModal}
        removeItem={removeItem}
        removeItemModalFn={removeItemModalFn}
        mode={props.mode}
        translation={translation}
        filteredWord={filteredWord}
        setSearch={setSearch}
        handleCloseIcon={handleCloseIcon}
        handleSearchChange={handleSearchChange}
        searchInput={searchInput}
        setSearchInput={setSearchInput}
        isCloseIcon={isCloseIcon}
        loading={loading}
      />
      <VocabModal
        isAddOrEdit={isAddOrEdit}
        modalVisible={modalVisible}
        openModal={openModal}
        addData={addData}
        editData={editData}
        setName={setName}
        setTag={setTag}
        tagArray={tagArray}
        name={name}
        translation={translation}
        setTranslation={setTranslation}
        tag={tag}
        mode={props.mode}
        duplicatedWord={duplicatedWord}
      />
      <RemoveItemModal
        isRemoveItemModal={isRemoveItemModal}
        removeItemModalFn={removeItemModalFn}
        removeItem={removeItem}
        name={name}
        id={id}
        mode={props.mode}
      />
      <PlusIcon 
        openModal={openModal}
        mode={props.mode}
      />
    </View>
  );
};

export default Home;
