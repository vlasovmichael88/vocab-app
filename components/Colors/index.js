export default AppColors = {
  light: {
    default: '#1F2544',
    secondary: '#474F7A',
    primary: '#81689D',
    white: '#FFFFFF',
    black: '#000000',
    red: '#ed5c4c',
    gray: '#333333',
    green: '#4CAF50',
  },
  dark: {
    default: '#E0DACB', // Inverted from #1F2544
    secondary: '#B8B085', // Inverted from #474F7A
    primary: '#7E9762', // Inverted from #81689D
    white: '#1C2128', // Inverted from #FFFFFF
    black: '#FFFFFF', // Inverted from #000000
    red: '#12A3B3', // Inverted from #ed5c4c
    gray: '#CCCCCC', // Inverted from #333333
    green: '#B3F2BF', // Inverted from #4CAF50
  },
};
