import { getColor } from '../utils';

const WordItemStyles = (props) => ({
  title: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleText: {
    fontSize: 24,
    paddingRight: 15,
    width: '100%',
    color: getColor(props.mode, 'default'),
  },
  item: {
    borderBottomColor: '#bbb',
    borderBottomWidth: 1,
    padding: 20,
    paddingTop: 10,
    paddingBottom: 10,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemText: {
    flex: 1,
  },
  num: {
    color: getColor(props.mode, 'default'),
    fontSize: 12,
    marginRight: 5,
  },
  tag: {
    fontSize: 14,
    color: getColor(props.mode, 'primary'),
  },
  translatedWord: {
    fontSize: 18,
    color: getColor(props.mode, 'default'),
    marginTop: 5,
    marginBottom: 5,
  },
});

export default WordItemStyles;
