import { Pressable, View, Text } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { getColor } from '../utils';
import WordItemStyles from './styles';

const WordItem = ({ ...props }) => {
  const styles = WordItemStyles(props);

  return (
    <View style={styles.item} key={props.wordItem}>
      <Pressable
        onPress={() =>
          props.openEditModal(
            props.wordItem,
            props.wordItemObj.name,
            props.wordItemObj.translation,
            props.wordItemObj.tag
          )
        }
        style={styles.itemText}
      >
        <View style={styles.title}>
          <Text style={styles.num}>{props.wordItemObj.wordCount}</Text>
          <Text style={styles.titleText}>{props.wordItemObj.name}</Text>
        </View>
        <Text style={styles.translatedWord}>
          {props.wordItemObj.translation || props.translation}
        </Text>
        {props.wordItemObj.tag ? (
          <Text style={styles.tag}>{'#' + props.wordItemObj.tag}</Text>
        ) : null}
      </Pressable>
      <Pressable
        onPress={() =>
          props.removeItemModalFn(props.WordItem, props.wordItemObj.name)
        }
      >
        <MaterialCommunityIcons
          name="trash-can"
          color={getColor(props.mode, 'red')}
          size={26}
        />
      </Pressable>
    </View>
  );
};

export default WordItem;
