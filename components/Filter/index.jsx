import {
    Pressable,
    View,
    StyleSheet,
    Text,
    ActivityIndicator,
  } from 'react-native';
import { getColor } from '../utils';

  import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
  
  const Filter = ({ ...props }) => {
    const styles = StyleSheet.create({
      title: {
        flexDirection: 'row',
        alignItems: 'center',
      },
     
    });
  
    return (
      <View style={styles.item}>
        <Pressable
            // onPress={() =>
            // props.removeItemModalFn(props.WordItem, props.wordItemObj.name)
            // }
        >
        <MaterialCommunityIcons
          name="filter"
          color={getColor(props.mode, 'red')}
          size={26}
        />
      </Pressable>
      </View>
    );
  };
  
  export default Filter;
  