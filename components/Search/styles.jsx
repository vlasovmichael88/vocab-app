import { getColor } from '../utils';

const SearchStyles = (props) => ({
  searchConainer: {
    marginRight: 20,
    marginLeft: 20,
    marginBottom: 30,
    marginTop: 10,
    flexDirection: 'row',
  },
  searchButton: {
    borderRadius: 20,
    elevation: 2,
    padding: 10,
    backgroundColor: getColor(props.mode, 'default'),
  },
  searchInput: {
    height: 60,
    fontSize: 18,
    borderWidth: 1,
    borderColor: getColor(props.mode, 'default'),
    padding: 10,
    paddingRight: 60,
    width: '100%',
    color: getColor(props.mode, 'black'),
    borderRadius: 10,
  },
  closeIcon: {
    position: 'absolute',
    top: 9,
    right: 15,
    padding: 8,
  },
});

export default SearchStyles;
