import { Pressable, TextInput, View } from 'react-native';
import { getColor } from '../utils';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SearchStyles from './styles';

const Search = ({ ...props }) => {
  const styles = SearchStyles(props);

  return (
    <View style={styles.searchConainer}>
      <TextInput
        value={props.searchInput}
        onChangeText={props.handleSearchChange}
        placeholderTextColor={getColor(props.mode, 'default')}
        style={styles.searchInput}
        placeholder="Search a word"
      />
      {props.isCloseIcon && (
        <Pressable onPress={props.handleCloseIcon} style={styles.closeIcon}>
          <MaterialCommunityIcons
            name="close"
            color={getColor(props.mode, 'default')}
            size={26}
          />
        </Pressable>
      )}
    </View>
  );
};

export default Search;
