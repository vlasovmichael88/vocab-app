import React, { useState, useEffect } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import { db } from '../../firebase-config';
import { ref, onValue } from 'firebase/database';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { getColor } from '../utils';
import StatisticsModalStyles from './styles';
const Statistics = ({...props}) => {
  const { stats, mode, results } = props;
  const styles = StatisticsModalStyles(props);

  const [lastEntry, setLastEntry] = useState(null);
  
  useEffect(() => {
      const dbRef = ref(db, '/statistics');
  
      // Listening for changes to the statistics node
      const unsubscribe = onValue(dbRef, (snapshot) => {
        if (snapshot.exists()) {
          const data = snapshot.val();
          setLastEntry(data); // Set the entire statistics object
        } else {
          console.log("No data available.");
          setLastEntry(null); // Clear the state if no data
        }
      });
  
      // Clean up the listener
      return () => unsubscribe();
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.view}>
        <Text style={styles.title}>Statistics</Text>
        {lastEntry ? (
          <>
            <Text style={styles.text}>Total Attempts: {lastEntry.attempts}</Text>
            <Text style={styles.text}>Correct Answers: {lastEntry.correctCount}</Text>
            <Text style={styles.text}>Incorrect Answers: {lastEntry.attempts - lastEntry.correctCount}</Text>
            <Text style={styles.text}>
              Accuracy: {((lastEntry.correctCount / lastEntry.attempts) * 100).toFixed(0)}%
            </Text>
          </>
        ) : (
          <ActivityIndicator size="large" />
        )}
      </View>
    </View>
  );
};

export default Statistics;
