import { getColor } from '../utils';

const StatisticsModalStyles = (props) => ({
  container: {
    backgroundColor: getColor(props.mode, 'white'),
    height: '100%',
  },
  view: {
    height: '100%',
    padding: 20,
    paddingTop: 40,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 20,
    color: getColor(props.mode, 'black'),
  },
  text: {
    color: getColor(props.mode, 'black'),
    fontSize: 18,
    marginBottom: 10,
  },
});

export default StatisticsModalStyles;
