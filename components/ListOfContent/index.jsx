import React from 'react';
import { 
  View,
  Text,
  ScrollView
} from 'react-native';
import ListOfContentStyles from './styles';

const ListOfContent = ({ ...props }) => {
  const styles = ListOfContentStyles(props);

  return (
    <ScrollView>
      <View style={styles.modalView}>
        <View style={styles.table}>
          {Object.values(props.word).map((item, index) => (
            <View
              key={index}
              style={[
                styles.tableRow,
                index % 2 === 0 && styles.evenChild
              ]}
            >
              <View style={[
                styles.tableCol,
                styles.borderRight,
              ]}>
                <Text style={styles.tableColText}>{item.name}</Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableColText}>{item.translation}</Text>
              </View>
            </View>
          ))}
        </View>
      </View>
    </ScrollView>
)};

export default ListOfContent;
