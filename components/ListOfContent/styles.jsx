import { getColor } from '../utils';

const ListOfContentStyles = (props) => ({
  modalView: {
    padding: 20,
    paddingTop: 40,
    backgroundColor: getColor(props.mode, 'white'),
  },
  table: {
    borderWidth: 1,
    borderColor: getColor(props.mode, 'default'),
    overflow: 'hidden',
  },
  tableRow: {
    flexDirection: 'row',
  },
  tableCol: {
    flex: 1,
    borderColor: getColor(props.mode, 'default'),
    borderWidth: 1,
    padding: 10,
  },
  tableColText: {
    color: getColor(props.mode, 'black'),
    fontSize: 20,
    wordBreak: 'break-word'
  },
  evenChild: {
    // backgroundColor: getColor(props.mode, 'gray'),
  },
  borderRight: {
    borderRightColor: getColor(props.mode, 'default'),
    borderRightWidth: 1,
  },
});

export default ListOfContentStyles;
