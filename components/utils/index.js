import AppColors from '../Colors';

export const debounce = (func, delay) => {
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => func(...args), delay);
  };
};

export const getColor = (mode, colorKey) => 
  mode === 'light' ? AppColors.light[colorKey] : AppColors.dark[colorKey];
