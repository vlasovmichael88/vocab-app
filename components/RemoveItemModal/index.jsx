import { Pressable, View, StyleSheet, Text, Modal } from 'react-native';
import { getColor } from '../utils';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import RemoveItemStyles from './styles';

const removeItemModal = ({ ...props }) => {
  const styles = RemoveItemStyles(props);

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={props.isRemoveItemModal}
      onRequestClose={props.removeItemModalFn}
    >
      <View style={styles.modalContainer}>
        <View style={styles.modalView}>
          <Text style={styles.removeTitle}>
            Do you really want to remove {'\n'}
            <Text style={styles.bold}>{props.name}</Text>
          </Text>
          <Pressable
            style={[styles.button, styles.buttonSubmit]}
            onPress={() => props.removeItem(props.id)}
          >
            <Text style={styles.buttonSubmitText}>Remove</Text>
          </Pressable>
          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={props.removeItemModalFn}
          >
            <MaterialCommunityIcons
              name="close"
              color={getColor(props.mode, 'white')}
              size={32}
            />
          </Pressable>
        </View>
      </View>
    </Modal>
  );
};

export default removeItemModal;
