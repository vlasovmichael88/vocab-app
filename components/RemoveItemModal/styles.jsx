import { getColor } from '../utils';
import stylesModal from '../ModalStyles';

const RemoveItemStyles = (props) => ({
  ...stylesModal,
  bold: {
    fontWeight: 'bold',
  },
  buttonSubmit: {
    marginTop: 30,
    width: '100%',
    padding: 10,
    backgroundColor: getColor(props.mode, 'red'),
    borderRadius: 10,
  },
  buttonSubmitText: {
    fontSize: 24,
    textAlign: 'center',
    color: getColor(props.mode, 'white'),
  },
  removeTitle: {
    fontSize: 24,
    color: getColor(props.mode, 'default'),
    textAlign: 'center',
  },
  modalView: {
    width: '100%',
    height: '100%',
    backgroundColor: getColor(props.mode, 'white'),
    borderRadius: 20,
    padding: 35,
    paddingTop: 100,
    marginTop: 50,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  buttonClose: {
    backgroundColor: getColor(props.mode, 'default'),
    borderRadius: 50,
    position: 'absolute',
    right: 20,
    top: 20,
  },
});

export default RemoveItemStyles;
