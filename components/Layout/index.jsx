import { useState, useEffect, useCallback } from 'react';
import { View, ActivityIndicator } from 'react-native';
import { db } from '../../firebase-config';
import { ref, onValue, push, update, remove, get } from 'firebase/database';
import { showMessage } from 'react-native-flash-message';
import { debounce } from '../utils';
import LayoutStyles from './styles';
import AppColors from '../Colors';

import Header from '../Header';
import Home from '../Home';
import Quiz from '../Quiz';
import Statistics from '../Statistics';
import ListOfContent from '../ListOfContent';

import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Layout = ({ ...props }) => {
  const { mode } = props;
  const [modalVisible, setModalVisible] = useState(false);
  const [isRemoveItemModal, setIsRemoveItemModal] = useState(false);
  const [loading, setLoading] = useState(true);
  const [name, setName] = useState('');
  const [translation, setTranslation] = useState('');
  const [tag, setTag] = useState('');
  const [wordCount, setWordCount] = useState(0);
  const [word, setWord] = useState({});
  const [isAddOrEdit, setIsAddOrEdit] = useState(true);
  const [uid, setUid] = useState('');
  const [tagArray, setTagArray] = useState([]);
  const [search, setSearch] = useState('');
  const [searchInput, setSearchInput] = useState('');
  const [isCloseIcon, setIsCloseIcon] = useState(false);
  const wordsKeys = word ? Object.keys(word) : [];
  const [quizStats, setQuizStats] = useState({
    attempts: 0,
    correctCount: 0,
  });
  const NOTIFICATION_DELAY = 3000;
  const showMessageSettings = {
    duration: NOTIFICATION_DELAY,
    icon: 'auto',
  };
  const styles = LayoutStyles(props);
  const Tab = createBottomTabNavigator();

  const calculateStats = () => {
    const totalAttempts = quizStats.attempts;
    const correct = quizStats.correctCount;
    const incorrect = totalAttempts - correct;
    const accuracy = totalAttempts ? ((correct / totalAttempts) * 100).toFixed(0) : 0;

    return { totalAttempts, correct, incorrect, accuracy };
  };

  const saveStatsToFirebase = async (stats) => {
    try {
      const statsRef = ref(db, '/statistics');
      
      // Get the existing statistics from Firebase
      const snapshot = await get(statsRef);
      const existingStats = snapshot.exists() ? snapshot.val() : {};

      // Merge the existing stats with the new stats
      const updatedStats = {
        ...existingStats,
        ...stats, // This will merge new stats with the existing ones
      };

      // Update Firebase with the merged stats
      await update(statsRef, updatedStats);
    } catch (error) {
      console.error('Error saving statistics:', error);
    }
  };

  const handleQuizCompletion = (isCorrect) => {
    setQuizStats((prevStats) => {
      // Update the local state based on the answer correctness
      const updatedStats = {
        attempts: prevStats.attempts + 1,
        correctCount: prevStats.correctCount + (isCorrect ? 1 : 0),
      };

      // Save the updated stats to Firebase
      saveStatsToFirebase(updatedStats);

      // Return the updated state
      return updatedStats;
    });
  };


  const addData = () => {
    const newWordCount = wordCount + 1;

    if (name === '') {
      showMessage({
        message: 'Error',
        description: 'This field cannot be empty. Please enter a word',
        type: 'danger',
        ...showMessageSettings,
      });
      return;
    }

    setIsAddOrEdit(true);

    push(ref(db, '/vocabulary'), {
      name,
      translation,
      tag,
      wordCount: newWordCount,
    });

    setName('');
    setTranslation('');
    setTag('');
    setWordCount(newWordCount);

    showMessage({
      message: 'Success',
      description: `The word ${name} was added successfully`,
      type: 'success',
      ...showMessageSettings,
    });
  };

  const openEditModal = (id, _name, _translation, _tag) => {
    setModalVisible(!modalVisible);
    setIsAddOrEdit(false);
    setName(_name);
    setTranslation(_translation);
    setTag(_tag);
    setUid(id);
  };

  const editData = () => {
    setIsAddOrEdit(false);

    update(ref(db, `/vocabulary/${uid}`), {
      name,
      translation,
      tag,
    });

    setModalVisible(!modalVisible);
  };

  const openModal = () => {
    setModalVisible(!modalVisible);
    setIsAddOrEdit(true);
    setName('');
    setTranslation('');
    setTag('');
  };

  const removeItemModalFn = (_id, _name) => {
    setIsRemoveItemModal(!isRemoveItemModal);
    setName(_name);
    setUid(_id);
  };

  const removeItem = async (id) => {
    await remove(ref(db, `/vocabulary/${id}`));

    // Fetch the updated vocabulary list from Firebase
    onValue(ref(db, '/vocabulary'), (querySnapShot) => {
      let data = querySnapShot.val() || {};

      // Reorder wordCount for all remaining items
      const reorderedData = Object.entries(data).map(([key, item], index) => ({
        ...item,
        wordCount: index + 1,
      }));

      // Update each item in Firebase with the new wordCount
      reorderedData.forEach((item, index) => {
        const itemRef = ref(db, `/vocabulary/${Object.keys(data)[index]}`);
        update(itemRef, {
          wordCount: item.wordCount,
        });
      });

      // Update the local state with the reordered data
      setWord({ ...data });
      setWordCount(Object.keys(data).length);
    });

    setIsRemoveItemModal(!isRemoveItemModal);
  };

  const handleSearchChange = (value) => {
    const valueLength = value.length > 0;
    valueLength ? setIsCloseIcon(true) : setIsCloseIcon(false);

    setSearchInput(value);
    setSearch(value);
  };

  const filteredWord = Object.entries(word).filter(([id, item]) => {
    return item.name.toLowerCase().includes(search.toLowerCase());
  });

  const handleCloseIcon = () => {
    setSearchInput('');
    setIsCloseIcon(false);
    handleSearchChange('');
  };

  const translateText = async (text, targetLanguage) => {
    try {
      const response = await fetch(
        `https://api.mymemory.translated.net/get?q=${encodeURIComponent(text)}&langpair=en|${targetLanguage}`
      );
  
      const data = await response.json();
  
      if (data && data.responseData && data.responseData.translatedText) {
        // Process the translation as needed
        const res = data.responseData.translatedText;
        setTranslation(res);
      } else {
        throw new Error('Translation not available');
      }
    } catch (error) {
      console.error('Translation error:', error);
      showMessage({
        message: 'Translation Error',
        description: 'Failed to fetch the translation. Please try again.',
        type: 'danger',
        ...showMessageSettings,
      });
      setTranslation('Error');
    }
  };

  const translateTextDebounced = useCallback(
    debounce((text, targetLanguage) => {
      translateText(text, targetLanguage);
    }, 500),
    []
  );

  const duplicatedWord = (value) => {
    const isDublicated = Object.values(word).some(
      (item) => item.name.toLowerCase() === value.toLowerCase()
    );

    if (isDublicated) {
      setName('');
      showMessage({
        message: 'Error',
        description: `The word ${value} already exits`,
        type: 'danger',
        ...showMessageSettings,
      });
      return;
    }
    
    setName(value);
    translateTextDebounced(value, 'ru');
  };

  useEffect(() => {
    const unsubscribe = onValue(ref(db, '/vocabulary'), (querySnapShot) => {
      const data = querySnapShot.val() || {};
  
      // Extract tags and names while constructing reorderedData
      const arrayOfTags = new Set();
      const reorderedData = Object.entries(data).map(([, item], index) => {
        arrayOfTags.add(item.tag?.trim()); // Avoid duplicates during iteration
        return {
          ...item,
          wordCount: index + 1,
        };
      });
  
      // Update items in Firebase only if the wordCount has changed
      reorderedData.forEach((item, index) => {
        const key = Object.keys(data)[index];
        if (data[key]?.wordCount !== item.wordCount) {
          const itemRef = ref(db, `/vocabulary/${key}`);
          update(itemRef, { wordCount: item.wordCount });
        }
      });
  
      // Convert tags Set to an array
      const filteredArrayOfTags = Array.from(arrayOfTags).filter(Boolean);

      setTagArray(filteredArrayOfTags);
      setWord({ ...data });
      setWordCount(Object.keys(data).length);
      setLoading(false);
    });
  
    return () => unsubscribe();
  }, []);

  if (loading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  return (
    <View style={styles.centeredView}>
      <View style={styles.container}>
        <Header
          text={'Vocabulary Bank'}
          mode={props.mode}
          setMode={props.setMode}
          setQuizVisible={props.setQuizVisible}
          isStatsModalVisible={props.isStatsModalVisible}
          setIsStatsModalVisible={props.setIsStatsModalVisible}
          menuVisible={props.menuVisible}
          setMenuVisible={props.setMenuVisible}
        />
        <NavigationContainer>
          <Tab.Navigator
            initialRouteName="Home"
            screenOptions={({ route }) => ({
              
              tabBarIcon: ({ color, size }) => {
                let iconName;

                if (route.name === 'Home') {
                  iconName = 'home';
                } else if (route.name === 'Quiz') {
                  iconName = 'gamepad-variant';
                } else if (route.name === 'Statistics') {
                  iconName = 'align-vertical-bottom';
                } else if (route.name === 'Translations') {
                  iconName = 'translate';
                }

                // Return the appropriate icon
                return (
                  <MaterialCommunityIcons
                    name={iconName}
                    color={color}
                    size={size}
                  />
                );
              },
              tabBarActiveTintColor: AppColors.light.primary,
              tabBarInactiveTintColor: AppColors.light.default,
            })}
          >
            <Tab.Screen
              name="Home"
              options={{
                headerShown: false,
              }}
            >
              {(props) => (
                <Home
                  wordsKeys={wordsKeys}
                  word={word}
                  openEditModal={openEditModal}
                  removeItem={removeItem}
                  removeItemModalFn={removeItemModalFn}
                  isAddOrEdit={isAddOrEdit}
                  modalVisible={modalVisible}
                  openModal={openModal}
                  addData={addData}
                  editData={editData}
                  setName={setName}
                  setTag={setTag}
                  tagArray={tagArray}
                  setTranslation={setTranslation}
                  tag={tag}
                  name={name}
                  duplicatedWord={duplicatedWord}
                  isRemoveItemModal={isRemoveItemModal}
                  id={uid}
                  mode={mode}
                  translation={translation}
                  filteredWord={filteredWord}
                  setSearch={setSearch}
                  handleCloseIcon={handleCloseIcon}
                  handleSearchChange={handleSearchChange}
                  searchInput={searchInput}
                  setSearchInput={setSearchInput}
                  isCloseIcon={isCloseIcon}
                  loading={loading}
                />
              )}
            </Tab.Screen>
            <Tab.Screen
              name="Quiz"
              options={{
                headerShown: false,
              }}
            >
              {() => (
                <Quiz
                  words={word}
                  handleQuizCompletion={handleQuizCompletion}
                  mode={mode}
                />
              )}
            </Tab.Screen>
            <Tab.Screen
              name="Translations"
              options={{
                headerShown: false,
              }}
            >
              {() => (
                <ListOfContent
                  word={word}
                  handleQuizCompletion={handleQuizCompletion}
                  mode={mode}
                />
              )}
            </Tab.Screen>
            <Tab.Screen
              name="Statistics"
              options={{
                headerShown: false,
              }}
            >
              {() => (
                <Statistics
                  stats={calculateStats()}
                  results={handleQuizCompletion}
                  mode={mode}
                />
              )}
            </Tab.Screen>
          </Tab.Navigator>
        </NavigationContainer>
      </View>
    </View>
  );
};

export default Layout;
