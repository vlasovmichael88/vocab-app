import { getColor } from '../utils';
import { StatusBar } from 'react-native';

const LayoutStyles = (props) => ({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
    width: '100%',
    height: '100%',
    backgroundColor: getColor(props.mode, 'white'),
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    backgroundColor: getColor(props.mode, 'white'),
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: getColor(props.mode, 'default'),
    position: 'absolute',
    bottom: 60,
    right: 30,
    borderRadius: 50,
  },
  empty: {
    color: getColor(props.mode, 'primary'),
    fontSize: 22,
    textAlign: 'center',
  },
});

export default LayoutStyles;
