import { Pressable } from 'react-native';
import { getColor } from '../utils';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import PlusIconStyles from './styles';

const PlusIcon = (props) => {
  const styles = PlusIconStyles(props);

  return (
    <Pressable
      style={[styles.button, styles.buttonOpen]}
      onPress={props.openModal}
    >
      <MaterialCommunityIcons
        name="plus"
        color={getColor(props.mode, 'white')}
        size={40}
      />
    </Pressable>
  );
};

export default PlusIcon;
