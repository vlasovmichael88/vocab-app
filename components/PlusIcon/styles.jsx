import { getColor } from '../utils';

const PlusIconStyles = (props) => ({
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: getColor(props.mode, 'default'),
    position: 'absolute',
    bottom: 60,
    right: 30,
    borderRadius: 50,
  },
});

export default PlusIconStyles;
