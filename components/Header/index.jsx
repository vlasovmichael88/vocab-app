import { Text, View } from 'react-native';
import LightDarkMode from '../LightDarkMode';
import HeaderStyles from './styles';
const Header = ({ ...props }) => {
  const styles = HeaderStyles(props);

  return (
    <View style={styles.headerContainer}>
      <Text style={styles.header}>{props.text}</Text>
      <LightDarkMode mode={props.mode} setMode={props.setMode} />
    </View>
  );
};

export default Header;
