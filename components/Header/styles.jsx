import { getColor } from '../utils';

const HeaderStyles = (props) => ({
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '10px 20px',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    zIndex: 10,
  },
  header: {
    fontSize: 26,
    flex: 1,
    color: getColor(props.mode, 'default'),
  },
});

export default HeaderStyles;
