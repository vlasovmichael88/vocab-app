import { View, ActivityIndicator, Text, ScrollView } from 'react-native';
import { getColor } from '../utils';
import WordItem from '../WordItem';
import Search from '../Search';
import Filter from '../Filter';
import WordListStyles from './styles';

const WordList = ({...props}) => {
  const styles = WordListStyles(props);

  return (
    <ScrollView>
      <View style={styles.container}>
        <>
        <Search
          setSearch={props.setSearch}
          mode={props.mode}
          filteredWord={props.filteredWord}
          handleSearchChange={props.handleSearchChange}
          setSearchInput={props.setSearchInput}
          handleCloseIcon={props.handleCloseIcon}
          isCloseIcon={props.isCloseIcon}
          searchInput={props.searchInput}
        />
        <Text style={styles.count}>
          Number of words in the dictionary: <Text style={styles.length}>{props.wordsKeys.length}</Text>
        </Text>
        </>
        {props.filteredWord.length > 0 ? (
          props.filteredWord.map(([id, key], num) => (
            <WordItem
              key={id}
              num={num}
              wordsKeys={props.wordsKeys}
              openEditModal={() => props.openEditModal(id, key.name, key.translation, key.tag)}
              removeItem={props.removeItem}
              removeItemModalFn={() => props.removeItemModalFn(id, key.name)}
              wordItemObj={key}
              mode={props.mode}
              translation={props.translation}
            />
          ))
        ) : props.loading ? (
          <View>
            <ActivityIndicator
              size="large"
              color={getColor(props.mode, 'default')}
            />
          </View>
        ) : (
          <Text style={styles.text}>
            No words found
          </Text>
        )
      }
    </View>
    </ScrollView>
  );
};

export default WordList;
