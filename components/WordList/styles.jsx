import { ImageBackground } from 'react-native';
import { getColor } from '../utils';

const WordListStyles = (props) => ({
  container: {
    backgroundColor: getColor(props.mode, 'white'),
  },
  text: {
    fontSize: 18,
    marginTop: 20,
    textAlign: 'center',
    color: getColor(props.mode, 'default'),
  },
  count: {
    fontSize: 14,
    marginLeft: 20,
    marginBottom: 5,
    color: getColor(props.mode, 'default'),
  },
  length: {
    marginLeft: 5,
    fontWeight: 'bold',
  },
});

export default WordListStyles;
