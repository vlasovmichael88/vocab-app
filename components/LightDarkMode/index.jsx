import { Pressable } from 'react-native';
import AppColors from '../Colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const LightDarkMode = ({ ...props }) => {
  return (
    <>
      {props.mode === 'light' ? (
        <Pressable onPress={() => props.setMode('dark')}>
          <MaterialCommunityIcons
            name="moon-waxing-crescent"
            color={AppColors.dark.white}
            size={30}
          />
        </Pressable>
      ) : (
        <Pressable onPress={() => props.setMode('light')}>
          <MaterialCommunityIcons
            name="white-balance-sunny"
            color={AppColors.light.white}
            size={30}
          />
        </Pressable>
      )}
    </>
  );
};

export default LightDarkMode;
