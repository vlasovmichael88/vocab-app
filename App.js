import { StyleSheet, View, Appearance } from 'react-native';
import { useState } from 'react';
import Layout from './components/Layout';
import { getColor } from './components/utils';
import FlashMessage from 'react-native-flash-message';

const colorScheme = Appearance.getColorScheme();

export default function App() {
  const [mode, setMode] = useState(colorScheme);
  const [quizVisible, setQuizVisible] = useState(false);
  const [isStatsModalVisible, setIsStatsModalVisible] = useState(false);
  const [menuVisible, setMenuVisible] = useState(false);
  const closeQuiz = () => setQuizVisible(false);

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      height: '100%',
      backgroundColor: getColor(mode, 'white'),
    },
  });

  return (
    <View style={styles.container}>
      <Layout 
        mode={mode}
        setMode={setMode}
        quizVisible={quizVisible}
        setQuizVisible={setQuizVisible}
        closeQuiz={closeQuiz}
        isStatsModalVisible={isStatsModalVisible}
        setIsStatsModalVisible={setIsStatsModalVisible}
        menuVisible={menuVisible}
        setMenuVisible={setMenuVisible}
      />
      <FlashMessage position="top" />
    </View>
  );
};
